create view empdept as
        select
                e.Fname,e.Lname,e.Salary,d.Dname,dl.LocationName
        from Employee as e
		INNER JOIN
	Department as d on d.Dnumber = e.EmpID
                INNER JOIN
        Dept_Locations as dl on dl.LNumber = e.EmpID
        order by e.EmpID asc;

