create table Dept_Locations
(
	Lnumber			int		not null,
	LocationName		varchar(15)	not null,
	PRIMARY KEY(Lnumber)
) default character set utf8 COLLATE utf8_general_ci;

create table Manager
(
	MNumber			int		not null,
	FName			varchar(30)	not null,
	LName			varchar(30)	not null,
	PersonalID		CHAR(13)	not null,
	Sex			CHAR,
	Bdate			DATE,
	ComFrom			varchar(30),
	PRIMARY KEY(MNumber)
) default character set utf8 COLLATE utf8_general_ci;

create table Department
(
	Dnumber			int		not null,
	Dname			varchar(15)	not null,
	ManagerID		int		not null,
	Mgr_start_date		DATE		not null,
	Location_id		int,
	PRIMARY KEY(Dnumber),
	FOREIGN KEY(Location_id) REFERENCES Dept_Locations(Lnumber),
	FOREIGN KEY(ManagerID) REFERENCES Manager(MNumber)
) default character set utf8 COLLATE utf8_general_ci;

create table Employee
(
	EmpID			int		not null,
	Fname			varchar(30)	not null,
	Lname			varchar(30)	not null,
	PersonalID		char(13)	not null,
	Bdate			DATE,
	Address			varchar(30),
	Sex			char,
	Salary			decimal(10,2),
	Dno			int		not null,

	PRIMARY KEY(EmpID),
	FOREIGN KEY (Dno) REFERENCES Department(Dnumber)
) default character set utf8 COLLATE utf8_general_ci;
