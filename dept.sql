create view dept as 
	select
		d.Dname,dl.LocationName
	from Department as d
		INNER JOIN
	Dept_Locations as dl on dl.Lnumber = d.Dnumber
	order by d.ManagerID asc;
