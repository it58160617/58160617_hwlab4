create view management as
        select
                d.Dnumber,d.Dname,m.Fname,m.Lname
        from Department as d
                INNER JOIN
	Manager as m on m.MNumber = d.Dnumber
        order by d.ManagerID asc;

