create view emp as
        select
                e.Fname,e.Lname,e.Salary,d.Dname
        from Employee as e
                INNER JOIN
        Department as d on d.DNumber = e.EmpID
        order by e.EmpID asc;

